export default {
  namespaced: true,
  state: {
    transitionName: {}
  },
  getters: {},
  actions: {
    setTransitionName({ commit }, payload) {
      commit("setTransitionName", payload);
    }
  },
  mutations: {
    setTransitionName(state, payload) {
      state.transitionName = payload;
    }
  }
};
